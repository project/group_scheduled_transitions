# Group Scheduled Transitions

## INTRODUCTION


The Group Scheduled Transitions module extends the Group module to support the
[Scheduled Transitions](https://www.drupal.org/project/scheduled_transitions)
functionality.

The code is developed based on issue
[#3219016](https://www.drupal.org/project/scheduled_transitions/issues/3219016)

This module adds Group support by adding 'view scheduled transitions' and
'add scheduled transitions' permissions. This is useful when you want certain
group roles to also to be able to scheduled transitions.

## REQUIREMENTS

* [Scheduled Transitions](https://www.drupal.org/project/scheduled_transitions)
* [Group](https://www.drupal.org/project/group).

## RECOMMENDED

This is not a requirements but we strongly recommend to also install the
[Group Content Moderation](https://www.drupal.org/project/gcontent_moderation)
module.


## INSTALLATION

The module can be installed via the
[standard Drupal installation process](https://drupal.org/node/1897420).

## CONFIGURATION

Go to the permissions page of your group types and set the new permissions
'view scheduled transitions' and 'add scheduled transitions'.

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).

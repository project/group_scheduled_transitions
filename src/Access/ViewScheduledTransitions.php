<?php

namespace Drupal\group_scheduled_transitions\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupContent;
use Symfony\Component\Routing\Route;

/**
 * Determines access to view scheduled transitions in a group context.
 */
class ViewScheduledTransitions implements AccessInterface {

  /**
   * Checks access for scheduled_transitions routes.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Symfony\Component\Routing\RouteMatchInterface $route_match
   *   The current route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $access_result = AccessResult::allowedIfHasPermission($account, 'bypass node access');

    if ($access_result->isAllowed()) {
      return AccessResult::allowed();
    }

    $parameters = $route_match->getParameters();

    if ($parameters->has('node')) {
      $node = $parameters->get('node');

      if ($group_contents = GroupContent::loadByEntity($node)) {
        /** @var \Drupal\group\Entity\GroupContent $group_content */
        foreach ($group_contents as $group_content) {
          $group = $group_content->getGroup();
          $access_result = GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'view scheduled transitions');

          if ($access_result->isAllowed()) {
            return AccessResult::allowed();
          }
        }
      }
    }

    return AccessResult::neutral();
  }

}
